The CSV, XLS, and XLSX files in the [original](./original) directory are the original datasets collected from [data.gov.bd](http://data.gov.bd/group/education).

The current version of these files are downloaded on 14 December 2021.
